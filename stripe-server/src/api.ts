import express, { NextFunction, Request, Response } from "express";
import cors from "cors";
import { createStripeCheckoutSession } from "./checkOut";
import { createPaymentIntend } from "./paymentIntents";
import { createStripeChargesSessionCharges } from "./charges";

export const app = express();

// middleware
app.use(express.json());
app.use(express.urlencoded());
app.use(cors({ origin: true }));
app.use(express.static("."));

app.get("/", (req, res) => {
  console.log("🚀 ~ file: api.ts ~ line 7 ~ app.get ~ req", req);

  res.send("<h1>Working In root url</h1>");
});

app.post(
  "/checkout",
  runAsync(async ({ body }: Request, res: Response) => {
    console.log("🚀 ~ file: api.ts ~ line 13 ~ runAsync ~ body", body);

    res.send(await createStripeCheckoutSession(body.line_items));
  })
);

app.post(
  "/payment",
  runAsync(async ({ body }: Request, res: Response) => {
    console.log("🚀 ~ file: api.ts ~ line 32 ~ runAsync ~ body", body);
    res.send(await createPaymentIntend(body.amount));
  })
);

// charges aip get card data and amount and pass to the createStripeChargesSessionCharges
// then that can accept payment
app.post(
  "/charge",
  runAsync(async ({ body }: Request, res: Response) => {
    res.send(await createStripeChargesSessionCharges(body));
  })
);

function runAsync(callback: Function) {
  return (req: Request, res: Response, next: NextFunction) => {
    callback(req, res, next).catch(next);
  };
}