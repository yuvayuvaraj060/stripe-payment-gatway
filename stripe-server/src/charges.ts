import { stripe } from "./";

export const createStripeChargesSessionCharges = async (paymentData: any) => {
console.log("🚀 ~ file: charges.ts ~ line 4 ~ createStripeChargesSessionCharges ~ paymentData", paymentData)
  const { amount, token, description, receipt_email } = paymentData;

  const session = await stripe.charges.create({
    amount: amount,
    source: token,
    description: description,
    receipt_email: receipt_email,
    currency: "inr",
  });
  return { session: session };
};
