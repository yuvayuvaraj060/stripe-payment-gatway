"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createStripeChargesSessionCharges = void 0;
const _1 = require("./");
const createStripeChargesSessionCharges = async (paymentData) => {
    console.log("🚀 ~ file: charges.ts ~ line 4 ~ createStripeChargesSessionCharges ~ paymentData", paymentData);
    const { amount, token, description, receipt_email } = paymentData;
    const session = await _1.stripe.charges.create({
        amount: amount,
        source: token,
        description: description,
        receipt_email: receipt_email,
        currency: "inr",
    });
    return { session: session };
};
exports.createStripeChargesSessionCharges = createStripeChargesSessionCharges;
//# sourceMappingURL=charges.js.map