import { Route, BrowserRouter as Router, Link, Switch } from "react-router-dom";
import "./App.css";
import CheckOut from "./CheckOut.js";
import Payments from "./Payment.js";
import ChargesAPI from "./charges/CheckOutForm.js";
function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <nav>
            <ul className="navbar-nav">
              <li>
                <Link to="/checkout">
                  <span aria-label="emoji" role="img">
                    🛒
                  </span>{" "}
                  Checkout
                </Link>
              </li>
              <li>
                <Link to="/payments">
                  <span aria-label="emoji" role="img">
                    💸
                  </span>{" "}
                  Payments
                </Link>
              </li>
            </ul>
          </nav>

          <main>
            <Switch>
              <Route path="/checkout">
                <CheckOut />
              </Route>
              <Route path="/payments">
                <Payments />
              </Route>
              <Route exact path="/" render={() => <ChargesAPI />} />
            </Switch>
          </main>
        </div>
      </Router>
    </div>
  );
}

export default App;
