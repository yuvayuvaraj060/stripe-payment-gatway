import React, { useState } from "react";
import { useStripe, useElements, CardElement } from "@stripe/react-stripe-js";

import CardSection from "./CaerdElemt";
import axios from "axios";

export default function CheckoutForm() {
  const stripe = useStripe();
  const elements = useElements();
  const [value, setValue] = useState({
    email: "",
    amount: Number,
  });
  const [success, setSuccess] = useState(false);

  const set = (name) => {
    console.log("🚀 ~ file: CheckOutForm.js ~ line 15 ~ set ~ name", name);
    return ({ target: { value } }) => {
      setValue((oldValue) => ({ ...oldValue, [name]: value }));
    };
  };
  const stripeTokenHandler = async (token) => {
    const paymentData = {
      token: token.id,
      email: value.email,
      amount: value.amount,
    };
    console.log(
      "🚀 ~ file: CheckOutForm.js ~ line 26 ~ stripeTokenHandler ~ paymentData",
      paymentData
    );

    const response = await axios.post("/charge", paymentData);
    console.log(
      "🚀 ~ file: CheckOutForm.js ~ line 29 ~ stripeTokenHandler ~ response",
      response
    );
    return response;
  };

  const handleSubmit = async (event) => {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault();

    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make  sure to disable form submission until Stripe.js has loaded.
      return;
    }

    const card = elements.getElement(CardElement);
    const result = await stripe.createToken(card);
    // const userData = event.c

    if (result.error) {
      // Show error to your customer.
      console.log(result.error.message);
    } else {
      // Send the token to your server.
      // This function does not exist yet; we will define it in the next step.
      const results = await stripeTokenHandler(result.token);
      if (results.data.session.status === "succeeded") {
        setSuccess(true);
        setValue({
          email: "",
          amount: Number,
        });
        card.clear();
      }
    }
    return;
  };

  return (
    <>
      <h3 style={{ textAlign: "center", margin: "20px" }}>ChargesAPI</h3>
      <div
        className="center"
        style={{ display: "grid", placeItems: "center", marginTop: "100px" }}
      >
        <form onSubmit={handleSubmit} id="card_form">
          <label htmlFor="email">Email Address:</label>
          <input
            type="text"
            value={value.email}
            id="email"
            placeholder="example@gmail.com"
            onChange={set("email")}
          />
          <label htmlFor="amount">Amount:</label>
          <input
            type="number"
            value={value.amount}
            id="amount"
            placeholder="example 600 rupees"
            onChange={set("amount")}
          />
          <CardSection />
          <button disabled={!stripe}>Confirm order</button>
          {success === true ? (
            <div
              style={{
                color: "blueviolet",
                margin: "20px",
                fontSize: "20",
                fontWeight: "bold",
                textAlign: "center",
              }}
            >
              Payment successful
            </div>
          ) : null}{" "}
        </form>
      </div>
    </>
  );
}
